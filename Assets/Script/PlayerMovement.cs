﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody rb;
    public float speed;
    public int count;
    public int maxcount;
    public Text countText;
    public float jumpforce;
    public bool grounded;
    public float fall;
    public float timer; 




    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
        count = 0;
        maxcount = GameObject.FindGameObjectsWithTag("Coin").Length;
        countText.text = "Count: " + count.ToString() + " / " + maxcount.ToString();
        countText.gameObject.SetActive(false);
        timer = 0;
    }

    void Update()
    {

        if (Input.GetKey("d"))
        {
            rb.AddForce(transform.right * speed * 10);
        }
        if (Input.GetKeyUp("d"))
        {
            rb.velocity = new Vector3(3.5f, 0,0);
        }
        if (Input.GetKey("a"))
        {
            rb.AddForce(-transform.right * speed * 10);
        }
        if (Input.GetKeyUp("a"))
        {
            rb.velocity = new Vector3(-3.5f,0,0);
        }
        if (Input.GetKeyDown("space")) 
        {
            Jump();
        }

        if (grounded == false)
        {
            rb.AddForce(-transform.up * fall * 10);
        }

        Timerupdate();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Coin"))
        {
            CancelInvoke("Timerupdate");
            other.gameObject.SetActive(false);
            count++;
            timer = Mathf.Round(timer * 100) * 0.01f;
            countText.text = "Count: " + count.ToString() + " / " + maxcount.ToString() + "   Time : " + timer.ToString();
           
        }

        if (other.gameObject.CompareTag("End"))
        {
            Time.timeScale = 0;
            countText.gameObject.SetActive(true);

        }
    }
    void Jump()
    {

        int layerMask = 1 << 8;

        layerMask = ~layerMask;

        RaycastHit hit;



        if (Physics.Raycast(transform.position, -transform.TransformDirection(Vector3.up), out hit, 0.7f, layerMask))
        {
            Debug.DrawRay(transform.position, -transform.TransformDirection(Vector3.up) * hit.distance, Color.yellow);
            if (hit.transform.tag == "ground")
            {
                rb.AddForce(transform.up * jumpforce * 10);
                
            }
        }
    }

    void Timerupdate()
    {
        timer = timer += Time.deltaTime;
    }
   
}

  






